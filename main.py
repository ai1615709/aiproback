from fastapi import FastAPI
from pydantic import BaseModel
import joblib

app = FastAPI()

# Load the trained model
model = joblib.load('ChurnModel.pkl')

class CusMeasurement(BaseModel):
    CustomerID: int
    Age: int
    Gender: int
    Tenure: int
    UsageFrequency: int
    SupportCalls: int
    PaymentDelay: int
    SubscriptionType: int
    ContractLength: int
    TotalSpend: int
    LastInteraction: int

class CusChurn(BaseModel):
    Churn: int

@app.post("/Cus/predict/")
async def predict_Churn(Cus: CusMeasurement):
    try:
        # Use the FishMeasurement object to predict the weight
        data = [[Cus.CustomerID,Cus.Age, Cus.Gender, Cus.Tenure, Cus.UsageFrequency, Cus.SupportCalls,Cus.PaymentDelay,Cus.SubscriptionType,Cus.ContractLength,Cus.TotalSpend,Cus.LastInteraction]]
        prediction = model.predict(data)
        
        # Create a FishWeight object to return the predicted weight
        predicted_Churn = CusChurn(Churn=prediction[0])
        
        return predicted_Churn
    except Exception as e:
        print("Error prediction:", e)
        return {"error": "Prediction failed"}
